package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;


public class Fridge implements IFridge{

    ArrayList<FridgeItem> itemList = new ArrayList<>();  // list for items in the fridge
    int maxSize = 20; // maximum capacity of fridge

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    
    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
            if (itemList.size() < maxSize){  // places item in the fridge if there is enough space left
                itemList.add(item);
                return true;
            } else {
                return false;
        }
    }
        
    

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (itemList.size()>0) {           // takes out item of fridge, but throws exception if the fridge is empty 
            itemList.remove(item);
        } else {
            throw new NoSuchElementException();

        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        itemList.removeAll(itemList);  // removes everything from the fridge 
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredList = new ArrayList<>(); // new list for expired food
        for (FridgeItem item : itemList){
            if (item.hasExpired() == true) {
                expiredList.add(item);  // adds expired food to the new list
                
            }
        }
        itemList.removeAll(expiredList); // removes the expired from fridge, and returs the list of expired food  
        return expiredList; 

    }
    
    
    
}
